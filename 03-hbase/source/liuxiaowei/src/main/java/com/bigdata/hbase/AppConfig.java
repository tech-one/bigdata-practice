package com.bigdata.hbase;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author xiaowei.liu
 */

public class AppConfig {

    public static String zkHost = null;
    public static String zkPort = null;

    static {
        Properties properties = new Properties();
        InputStream in = AppConfig.class.getClassLoader()
            .getResourceAsStream("application.properties");
        try {
            properties.load(in);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        zkHost = properties.getProperty("zkHost");
        zkPort = properties.getProperty("zkPort");
    }

}
