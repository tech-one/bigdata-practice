package com.bigdata.hbase.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author xiaowei.liu
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Student {

    String name;

    String studentId;

    String classId;

    String understanding;

    String programming;

}
