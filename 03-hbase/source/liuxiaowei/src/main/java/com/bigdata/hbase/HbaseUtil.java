package com.bigdata.hbase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptor;
import org.apache.hadoop.hbase.client.ColumnFamilyDescriptorBuilder;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.client.TableDescriptor;
import org.apache.hadoop.hbase.client.TableDescriptorBuilder;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * @author xiaowei.liu
 */
public class HbaseUtil {

    static Configuration configuration;
    static Admin admin;

    static {
        /**
         * 管理Hbase的配置信息
         */
        configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", AppConfig.zkHost);
        configuration.set("hbase.zookeeper.property.clientPort", AppConfig.zkPort);
    }

    /**
     * 获取 Hbase连接
     */
    public static Connection getConnection() throws IOException {
        return ConnectionFactory.createConnection(configuration);
    }

    /**
     * 关闭 Hbase连接
     */
    public static void close(Table table) throws IOException {
        if (null != table) {
            table.close();
        }
    }

    /**
     * 关闭 Hbase连接
     */
    public static void close(Connection connection) throws IOException {
        if (null != connection && !connection.isClosed()) {
            connection.close();
        }
    }

    /**
     * 管理Hbase数据库的信息
     */
    public static Admin getAdmin(Connection connection) throws IOException {
        if (null == admin || admin.isAborted()) {
            return connection.getAdmin();
        }
        return admin;
    }

    /**
     * 创建 TableName
     */
    public static TableName createTableName(String tableName) {
        return TableName.valueOf(tableName);
    }

    /**
     * 删除表
     */
    public static void dropTable(Connection connection, TableName tableName) throws IOException {
        //Admin是操作表的类，具有创建
        Admin admin = getAdmin(connection);
        if (admin.tableExists(tableName)) {
            admin.disableTable(tableName);
            System.err.println("表停用完成!" + tableName);
            admin.deleteTable(tableName);
            System.err.println("表删除完成!" + tableName);
        }
    }

    /**
     * 创建表  https://blog.csdn.net/snihytn/article/details/105544169
     */
    public static void createTable(Connection connection, TableName tableName,
        List<String> familyNames) throws IOException {
        //删除旧表
        dropTable(connection, tableName);

        //列族  HColumnDescriptor已废弃
        List<ColumnFamilyDescriptor> familyDescriptors = new ArrayList<>();
        for (String family : familyNames) {
            ColumnFamilyDescriptor columnFamilyDescriptor = ColumnFamilyDescriptorBuilder
                .newBuilder(family.getBytes()).build();
            familyDescriptors.add(columnFamilyDescriptor);
        }

        //表   HTableDescriptor已废弃
        TableDescriptor tableDescriptor = TableDescriptorBuilder
            .newBuilder(tableName)
            .setColumnFamilies(familyDescriptors)
            .build();

        //执行创建
        getAdmin(connection).createTable(tableDescriptor);
    }


    /**
     * 获取Table对象
     */
    public static Table getTable(Connection connection, TableName tableName) throws IOException {
        Table table = connection.getTable(tableName);
        return table;
    }


    /**
     * 添加数据
     */
    public static void addRowData(Table table, String rowkey,
        String colFamily, String col, String value) throws IOException {
        Put put = new Put(rowkey.getBytes());
        put.addColumn(colFamily.getBytes(), col.getBytes(), value.getBytes());
        table.put(put);
    }

    /**
     * 添加数据
     */
    public static void addRowData(Connection connection, TableName tableName, String rowkey,
        String colFamily, String col, String value) throws IOException {
        Put put = new Put(rowkey.getBytes());
        put.addColumn(colFamily.getBytes(), col.getBytes(), value.getBytes());

        Table table = getTable(connection, tableName);
        table.put(put);
    }


    /**
     * 删除数据
     */
    public static void deleteRowData(Connection connection, TableName tableName, String rowkey)
        throws IOException {
        Delete delete = new Delete(Bytes.toBytes(rowkey));

        Table table = connection.getTable(tableName);
        table.delete(delete);
    }

    /**
     * 删除数据
     */
    public static void deleteRowData(Connection connection, TableName tableName, String rowkey,
        String colFamily, String col) throws IOException {

        Delete delete = new Delete(Bytes.toBytes(rowkey));
        delete.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col));

        Table table = connection.getTable(tableName);
        table.delete(delete);
    }

    /**
     * 删除数据
     */
    public static void deleteRowData(Table table, String rowkey,
        String colFamily, String col) throws IOException {

        Delete delete = new Delete(Bytes.toBytes(rowkey));
        delete.addColumn(Bytes.toBytes(colFamily), Bytes.toBytes(col));

        table.delete(delete);
    }

    /**
     * 删除数据
     */
    public static void deleteRowData(Table table, String rowkey) throws IOException {
        Delete delete = new Delete(Bytes.toBytes(rowkey));
        table.delete(delete);
    }

    /**
     * scan 获取数据
     */
    public static void scanRows(Table table, String family) throws IOException {
        System.err.println("查询数据 scan!");
        //得到用于扫描 region 的对象
        Scan scan = new Scan();
        scan.addFamily(Bytes.toBytes(family));
        scan.addColumn(Bytes.toBytes(family), Bytes.toBytes("student_id"));
        //使用 Table 得到 resultcanner 实现类的对象
        ResultScanner resultScanner = table.getScanner(scan);
        for (Result result : resultScanner) {
            Cell[] cells = result.rawCells();
            for (Cell cell : cells) {
                //得到 rowkey
                System.out.println(" 行 键 :" + Bytes.toString(CellUtil.cloneRow(cell)));//得到列族
                System.out.println(" 列 族 " + Bytes.toString(CellUtil.cloneFamily(cell)));
                System.out.println(" 列 :" + Bytes.toString(CellUtil.cloneQualifier(cell)));
                System.out.println(" 值 :" + Bytes.toString(CellUtil.cloneValue(cell)));
            }
        }
    }

    public static void getRow(Table table, String rowKey) throws IOException {
        System.err.println("查询数据 get!");
        Get get = new Get(Bytes.toBytes(rowKey));
        //get.setMaxVersions();显示所有版本
        //get.setTimeStamp();显示指定时间戳的版本
        Result result = table.get(get);
        for (Cell cell : result.rawCells()) {
            System.out.println(" 行 键 :" +
                Bytes.toString(result.getRow()));
            System.out.println(" 列 族 " +
                Bytes.toString(CellUtil.cloneFamily(cell)));
            System.out.println(" 列 :" +
                Bytes.toString(CellUtil.cloneQualifier(cell)));
            System.out.println(" 值 :" +
                Bytes.toString(CellUtil.cloneValue(cell)));
            System.out.println("时间戳:" + cell.getTimestamp());
        }
    }
}
