package com.bigdata.hbase.test;

import com.bigdata.hbase.HbaseUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Table;

/**
 * @author xiaowei.liu
 */
public class DMLTest {

    static List<Student> students = new ArrayList<Student>(){
        {
            add(new Student("Tom","20210000000001","1","75","82"));
            add(new Student("Jerry","20210000000002","1","85","67"));
            add(new Student("Jack","20210000000003","2","80","80"));
            add(new Student("Rose","20210000000004","2","60","61"));
        }
    };

    static String studentTable = "liuxiaowei:student";
    static String colFamily0 = "info";
    static String col0 = "student_id";
    static String col1 = "class";
    static String colFamily1 = "score";
    static  String col2 = "understanding";
    static  String col3 = "programming";

    public static void main(String[] args) throws IOException {
        Connection connection =  null;
        Table table= null;
        try{
            connection  = HbaseUtil.getConnection();
            TableName tableName = HbaseUtil.createTableName(studentTable);
            table = HbaseUtil.getTable(connection,tableName);
            //删除数据
            deleteStudents(table,students);
            //添加数据
            addStudents(table,students);
            //查询数据 scan
            HbaseUtil.scanRows(table,"info");
            HbaseUtil.scanRows(table,"score");
            //查询数据 get
            HbaseUtil.getRow(table,"Jerry");
        }catch (Exception ex){
            ex.printStackTrace();
            System.err.println("hbase操作失败!"+ex.toString());
        }finally {
            HbaseUtil.close(table);
            HbaseUtil.close(connection);
        }
    }


    public static void addStudents(Table table, List<Student> students) throws IOException {
        for (Student student : students) {
            String rowkey = student.getName();
            //info
            HbaseUtil.addRowData(table,rowkey , colFamily0,col0, student.getStudentId());
            HbaseUtil.addRowData(table,rowkey , colFamily0,col1, student.getClassId());
            //score
            HbaseUtil.addRowData(table,rowkey , colFamily1,col2, student.getUnderstanding());
            HbaseUtil.addRowData(table,rowkey , colFamily1,col3, student.getProgramming());
        }
        System.err.println("添加完成！");
    }

    public static void deleteStudents(Table table, List<Student> students) throws IOException {
        for (Student student : students) {
            String rowkey = student.getName();
            HbaseUtil.deleteRowData(table , rowkey);
        }
        System.err.println("删除完成！");
    }
}
