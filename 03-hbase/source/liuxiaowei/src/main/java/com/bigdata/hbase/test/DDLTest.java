package com.bigdata.hbase.test;

import com.bigdata.hbase.HbaseUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;

/**
 * @author xiaowei.liu
 */
public class DDLTest {

    static String studentTable = "liuxiaowei:student";

    public static void main(String[] args) throws IOException {
        Connection connection =  null;
        try{
            connection  = HbaseUtil.getConnection();
            //创建表
            createStudentTable(connection);
            //打印 表
            TableName[] tableNames =  HbaseUtil.getAdmin(connection).listTableNames();
            Arrays.stream(tableNames)
                .forEach(System.out::println);
            Arrays.stream(tableNames)
                .filter(t->t.getNameAsString().equals(studentTable))
                .forEach(System.out::println);
        }catch (Exception ex){
            ex.printStackTrace();
            System.err.println("hbase操作失败!"+ex.toString());
        }finally {
            HbaseUtil.close(connection);
        }
    }

    public static void createStudentTable(Connection connection) throws IOException {
        TableName tableName = HbaseUtil.createTableName(studentTable);
        List<String> familyNames = new ArrayList<String>(){{ add("info");add("score");}};
        HbaseUtil.createTable(connection,tableName,familyNames);
        System.err.println("创建表完成....");
    }

}
