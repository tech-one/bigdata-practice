
# hosts文件：C:\Windows\System32\drivers\etc\hosts
# 增加配置 jike
47.101.206.249  jikehadoop01
47.101.216.12   jikehadoop02
47.101.204.23	jikehadoop03
47.101.202.85	jikehadoop04
47.101.72.185	jikehadoop05
139.196.15.153  jikehadoop06
106.15.39.86	jikehadoop07
139.196.162.22  jikehadoop08

# hbase 基本命令

# 命名空间
list_namespace
create_namespace 'liuxiaowei'
describe_namespace 'liuxiaowei'
drop_namespace 'liuxiaowei'

# 表
list
list_namespace_tables 'liuxiaowei'
describe  'liuxiaowei:student'
scan "liuxiaowei:student"
scan "liuxiaowei:student" , {STARTROW=>"Tom"}
get "liuxiaowei:student","Rose"

create  "liuxiaowei:student" ,"info","score"
disable "liuxiaowei:student"
drop  "liuxiaowei:student"

truncate "liuxiaowei:student"


