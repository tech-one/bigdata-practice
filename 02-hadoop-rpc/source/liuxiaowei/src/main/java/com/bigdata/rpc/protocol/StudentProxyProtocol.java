package com.bigdata.rpc.protocol;

import org.apache.hadoop.ipc.VersionedProtocol;

public interface StudentProxyProtocol extends VersionedProtocol {

    /**
     * 版本号，默认情况下，不同版本号的RPC Client和Server之间不能相互通信
     */
    long versionID =  10000L;

    String getStudentNo(String studentNo);
}