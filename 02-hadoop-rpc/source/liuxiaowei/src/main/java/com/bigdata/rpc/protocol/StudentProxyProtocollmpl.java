package com.bigdata.rpc.protocol;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.hadoop.ipc.ProtocolSignature;

/**
 * @author xiaowei.liu
 */
public class StudentProxyProtocollmpl implements StudentProxyProtocol {

    static Map<String,String> studentMap = new HashMap<>();
    static {
        studentMap.put("G20210735010412","刘晓伟");
        studentMap.put("20210123456789","心心");
    }

    @Override
    public String getStudentNo(String studentNo) {
        System.out.println("printStudentNo():我被调用了!");
        return studentMap.get(studentNo);
    }

    @Override
    public long getProtocolVersion(String protocol, long clientVersion) throws IOException {
        return StudentProxyProtocol.versionID;
    }

    @Override
    public ProtocolSignature getProtocolSignature(String protocol,
        long clientVersion, int clientMethodsHash) throws IOException {
        return new ProtocolSignature(StudentProxyProtocol.versionID,null);
    }
}
