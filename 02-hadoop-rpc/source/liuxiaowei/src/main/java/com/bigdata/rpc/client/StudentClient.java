package com.bigdata.rpc.client;

import com.bigdata.rpc.protocol.StudentProxyProtocol;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;

public class StudentClient {

    static int port = 8000;
    static String IPAddress = "127.0.0.1";

    public static void main(String[] args) throws IOException {
        StudentProxyProtocol proxy = RPC.getProxy(StudentProxyProtocol.class,
                StudentProxyProtocol.versionID,
            new InetSocketAddress(
                IPAddress, port),
                new Configuration());

        String[] array = new String[]{"G20210735010412","20210000000000","20210123456789"};
        for (String studentNo : array) {
            String result = proxy.getStudentNo(studentNo);
            System.out.println("调用远程接口:学号="+studentNo+",姓名="+result);
        }

        RPC.stopProxy(proxy);

    }

}