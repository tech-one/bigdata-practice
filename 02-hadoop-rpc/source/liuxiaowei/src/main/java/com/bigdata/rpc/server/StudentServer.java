package com.bigdata.rpc.server;

import com.bigdata.rpc.protocol.StudentProxyProtocol;
import com.bigdata.rpc.protocol.StudentProxyProtocollmpl;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.ipc.Server;

public class StudentServer {
     static int PORT = 8000;
     static String IPAddress = "127.0.0.1";

    public static void main(String[] args) throws Exception {
        Server server = new RPC.Builder(new Configuration())
            .setProtocol(StudentProxyProtocol.class)
            .setInstance(new StudentProxyProtocollmpl())
            .setBindAddress(IPAddress)
            .setPort(PORT)
            .build();
        server.start();
        System.out.println("server启动成功!");
    }
}