package com.bigdata.rpc;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import org.apache.hadoop.io.Writable;

/**
 * @author xiaowei.liu
 */
public class SerializeUtil {

    public  static byte[] serialize(Writable writable) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        DataOutputStream dataOut = new DataOutputStream(out);
        writable.write(dataOut);
        dataOut.close();
        return out.toByteArray();
    }

    public static   byte[] deserialize(Writable writable,byte[] bytes) throws IOException {
        ByteArrayInputStream in  = new ByteArrayInputStream(bytes);
        DataInputStream dataIn= new DataInputStream(in);
        writable.readFields(dataIn);
        dataIn.close();
        return bytes;
    }

}
