package com.bigdata.mapreduce;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * @author xiaowei.liu
 */
public class FlowCountMapper extends Mapper<LongWritable, Text, Text, FlowBean> {

    @Override
    protected void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {
        String line = value.toString();

        String[] array = line.split("\t", -1);
        String phone = array[1];

        long upFlow = Long.valueOf(array[7]);
        long downFlow = Long.valueOf(array[8]);

        FlowBean flowBean = new FlowBean(upFlow,downFlow);

        context.write(new Text(phone),flowBean);
    }
}
