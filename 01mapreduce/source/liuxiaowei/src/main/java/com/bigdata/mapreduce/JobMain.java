package com.bigdata.mapreduce;


import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/**
 * @author xiaowei.liu
 */
public class JobMain {

    public static void main(String[] args)
        throws IOException, ClassNotFoundException, InterruptedException {
        if(args.length != 2){
            System.err.println("args params error！");
            System.exit(1);
        }
        Path inputFile = new Path(args[0]);
        Path outFile = new Path(args[1]);

        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(configuration);
        if(fileSystem.exists(outFile)){
            // true的意思是，就算output有东西，也一并删除
            fileSystem.delete(outFile,true);
        }

        Job job = Job.getInstance(configuration, "flowcount");
        job.setJarByClass(JobMain.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        TextInputFormat.addInputPath(job,inputFile);
        TextOutputFormat.setOutputPath(job,outFile);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(FlowBean.class);


        job.setMapperClass(FlowCountMapper.class);
        job.setReducerClass(FlowCountReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(FlowBean.class);

        //三、等待完成
        boolean b = job.waitForCompletion(true);
        System.out.println("liuxiaowei job submit state:"+b);
        System.exit(b ? 0 : 1);

    }
}
