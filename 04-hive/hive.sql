
-- 作业1：展示电影ID为2116这部电影各年龄段的平均影评分
select 
  t1.age,avg(rate) as avgrate
 from t_user t1
 left join t_rating t2 on t1.userid = t2.userid
 where t2.movieid = 2116
 group by t1.age

-- 作业2：找出男性评分最高且评分次数超过50次的10部电影，展示电影名，平均影评分和评分次数
select 
  'M' as sex, t2.moviename,t1.avgrate,t1.total
from (
select t1.movieid, avg(rate) avgrate,count(*) as total
from t_rating t1
left join t_user t2 on t1.userid = t2.userid
where t2.sex = 'M'
group by t1.movieid
having total > 50
order by avgrate desc 
limit 10 
) t1
left join t_movie t2 on t1.movieid = t2.movieid

-- 作业3：找出影评次数最多的女士所给出最高分的10部电影的平均影评分，展示电影名和平均影评分（可使用多行SQL）
 
 -- 3.1 找出影评次数最多的女性,用户ID=1150
   SELECT userid from (
		SELECT 
		 t2.userid ,count(t1.userid) as total
		from  t_rating  t1
		left join t_user t2 on t1.userid = t2.userid
		where t2.sex = 'F'
		group by t2.userid
		order by  total desc 
		limit 1
    )t1

 -- 3.2 找出用户ID=1150的评分排行前10的电影名称及平均分	
	select t2.moviename,t1.avgrate
	from (
		select 
		  movieid, avg(rate) as avgrate 
		from t_rating 
		where movieid in (
			 SELECT movieid from (
			   SELECT
				  movieid,rate
				from t_rating  
				where userid = 1150
				order by rate desc 
				limit 10
			  )t
		 )
	   GROUP BY movieid
	) t1 left join t_movie t2 on t1.movieid = t2.movieid  


